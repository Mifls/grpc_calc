#include <iostream>
#include <string>
#include "Client.h"
#include "Server.h"
#include <thread>
#include <future>


int main(int argc, char** argv) {

    std::promise<void> exit_requested;
    std::unique_ptr<Server> *server;

    std::thread server_thread([&exit_requested, &server]() {
        std::string server_address("0.0.0.0:1234");
        CalcServiceImpl service;
        service.exit_requested = &exit_requested;
        ServerBuilder builder;
        builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
        builder.RegisterService(&service);
        server = new std::unique_ptr<Server>(builder.BuildAndStart());
        std::cout << "Server listening on " << server_address << std::endl;
        (*server)->Wait();
    });

    std::thread client_thread([]() {

        Client client(grpc::CreateChannel(
                "localhost:1234", grpc::InsecureChannelCredentials()));

        bool working = true;
        while (working) {
            char sign;
            int a, b;
            printf(">>>");
            scanf("%d%c%d", &a, &sign, &b);
            switch(sign) {
                case '+':
                    printf("%d\n", client.Sum(a, b));
                    break;
                case '-':
                    printf("%d\n", client.Diff(a, b));
                    break;
                case '*':
                    printf("%d\n", client.Mul(a, b));
                    break;
                case '/':
                    printf("%d\n", client.Div(a, b));
                    break;
                default:
                    client.Close();
                    working = false;
                    break;
            }
        }
    });

    auto waiting = exit_requested.get_future();
    waiting.wait();
    (*server)->Shutdown();

    client_thread.join();
    server_thread.join();
    return 0;
}