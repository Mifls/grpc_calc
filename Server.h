#ifndef GRPC_CALC_SERVER_H
#define GRPC_CALC_SERVER_H

#include <iostream>
#include <memory>
#include <string>
#include <grpc++/grpc++.h>
#include <future>
#include "calc.grpc.pb.h"

using namespace grpc;
using namespace calc;

class CalcServiceImpl final : public Calc::Service {
    Status Sum(ServerContext* context, const Request* request,
               Reply* reply) override {
        reply->set_ans(request->a() + request->b());
        return Status::OK;
    }

    Status Diff(ServerContext* context, const Request* request,
                Reply* reply) override {
        reply->set_ans(request->a() - request->b());
        return Status::OK;
    }

    Status Mul(ServerContext* context, const Request* request,
               Reply* reply) override {
        reply->set_ans(request->a() * request->b());
        return Status::OK;
    }

    Status Div(ServerContext* context, const Request* request,
               Reply* reply) override {
        reply->set_ans(request->a() / request->b());
        return Status::OK;
    }

    Status Close(ServerContext* context, const CloseRequest* request,
               CloseReply* reply) override {
        exit_requested->set_value();
        return Status::OK;
    }

public:
    std::promise<void> *exit_requested;
};

#endif //GRPC_CALC_SERVER_H
