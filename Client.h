#ifndef GRPC_CALC_CLIENT_H
#define GRPC_CALC_CLIENT_H

#include <iostream>
#include <memory>
#include <string>
#include <grpc++/grpc++.h>
#include "calc.grpc.pb.h"

using namespace grpc;
using namespace calc;

class Client {
public:
    explicit Client(const std::shared_ptr<Channel>& channel)
            : stub_(Calc::NewStub(channel)) {}

    int Sum(int a, int b) {
        Request request;
        request.set_a(a);
        request.set_b(b);
        Reply reply;
        ClientContext context;
        Status status = stub_->Sum(&context, request, &reply);
        if (status.ok()) {
            return reply.ans();
        } else {
            std::cout << status.error_code() << ": " << status.error_message()
                      << std::endl;
            return 0;
        }
    }

    int Diff(int a, int b) {
        Request request;
        request.set_a(a);
        request.set_b(b);
        Reply reply;
        ClientContext context;
        Status status = stub_->Diff(&context, request, &reply);
        if (status.ok()) {
            return reply.ans();
        } else {
            std::cout << status.error_code() << ": " << status.error_message()
                      << std::endl;
            return 0;
        }
    }

    int Mul(int a, int b) {
        Request request;
        request.set_a(a);
        request.set_b(b);
        Reply reply;
        ClientContext context;
        Status status = stub_->Mul(&context, request, &reply);
        if (status.ok()) {
            return reply.ans();
        } else {
            std::cout << status.error_code() << ": " << status.error_message()
                      << std::endl;
            return 0;
        }
    }

    int Div(int a, int b) {
        Request request;
        request.set_a(a);
        request.set_b(b);
        Reply reply;
        ClientContext context;
        Status status = stub_->Div(&context, request, &reply);
        if (status.ok()) {
            return reply.ans();
        } else {
            std::cout << status.error_code() << ": " << status.error_message()
                      << std::endl;
            return 0;
        }
    }

    int Close() {
        CloseRequest request;
        CloseReply reply;
        ClientContext context;
        Status status = stub_->Close(&context, request, &reply);
        if (status.ok()) {
            return 0;
        } else {
            std::cout << status.error_code() << ": " << status.error_message()
                      << std::endl;
            return -1;
        }
    }

private:
    std::unique_ptr<Calc::Stub> stub_;
};

#endif //GRPC_CALC_CLIENT_H
